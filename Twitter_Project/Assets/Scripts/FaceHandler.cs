﻿using UnityEngine;
using System.Collections;

public class FaceHandler : MonoBehaviour {

    public Texture2D[] allFaces;
    private Material mat;
    private Animation anim;
    public bool isBlake;
    public bool isChristina;
    public bool isAdam;
    public Gradient adamHair;
    private ParticleSystem[] sparkles;
	// Use this for initialization
	void Start () {
        if (!isBlake)
        {
            mat = GetComponent<Renderer>().material;
        }
        else {
            mat = transform.GetChild(2).GetComponent<Renderer>().material;
        }
        anim = GetComponent<Animation>();
        if (isChristina) {
            sparkles = GetComponentsInChildren<ParticleSystem>();
        }
      //  StartCoroutine("AnimateFace");
	}

    private void SwitchFace(int face) {
        mat.mainTexture = allFaces[face];
        if (isChristina) {
            foreach (ParticleSystem psys in sparkles) {
                psys.Play();
            }
        }
    }

    /*
    private IEnumerator AnimateFace() {
        anim.Play();
        for (int i = 0; i < allFaces.Length; i++) {
            mat.mainTexture = allFaces[i];
            yield return new WaitForSeconds(0.5f);
        }
        Debug.Log("About to loop coroutine again");
        StartCoroutine("AnimateFace");
        yield break;
    }
    */
	// Update is called once per frame
	void Update () {
	
	}
}
